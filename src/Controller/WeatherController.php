<?php

namespace App\Controller;

use App\Form\CityWeatherFormType;
use App\Service\ContentPartnerManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WeatherController extends AbstractController
{
    #[Route('/weather/city', name: 'app_weather_city')]
    public function index(ContentPartnerManager $contentPartnerManager, Request $request): Response
    {
        $arrayResponse = null;

        $form = $this->createForm(CityWeatherFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $city = $form['city']->getData();
            $json = $form['json']->getData();

            $arrayResponse = $contentPartnerManager->getWeatherForCity($city);

            if ($json) {
                return new JsonResponse($arrayResponse);
            }
        }


        $formView = $form->createView();
        return $this->render('weather/index.html.twig', [
            'form' => $formView,
            'data' => $arrayResponse,
        ]);
    }
}
