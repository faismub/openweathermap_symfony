<?php


namespace App\Utils;


class WeatherHelper
{
    public static function formatUnixTimeToDateTimeString($unixTime){
        return gmdate("d.m.Y H:i", $unixTime);
    }

    public static function formatUnixTimeToTimeString($unixTime){
        return gmdate("H:i", $unixTime);
    }
}