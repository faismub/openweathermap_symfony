<?php


namespace App\Service;


use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ContentPartner
{
    const WEATHER_API_BASE_URL = "https://api.openweathermap.org/data/2.5/weather?";

    private $client;
    private $errorLogger;
    private $url;

    public function __construct(HttpClientInterface $client, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->errorLogger = $logger;
        $this->url = self::WEATHER_API_BASE_URL."appid=" . $_ENV['OWM_API_KEY'];

        //preset Units of measurement below - see https://openweathermap.org/current#data
        $this->url .= "&lang=de"; //language
        $this->url .= "&units=metric"; //use metric system
    }

    public function getWeatherForCity($city)
    {
        $requestUrl = $this->url . "&q=" . $city;
        $cache = new FilesystemAdapter();

        try {

            $value = $cache->get($city, function (ItemInterface $item) use ($requestUrl){
                $item->expiresAfter(3600);

                $response = $this->client->request(
                    'GET',
                    $requestUrl,
                );

                if ($response->getStatusCode() == 200) {
                    return $response->toArray();
                } else {
                    return false;
                }
            });

            return $value;

        } catch (\HttpRequestException $exception) {
            $this->errorLogger->error('Could not request weather for city: ' . $exception->getMessage());
        }

        return false;
    }
}