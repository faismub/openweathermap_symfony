<?php


namespace App\Service;


use App\Utils\WeatherHelper;
use Psr\Log\LoggerInterface;

class ContentPartnerManager
{

    private $contentPartner;
    private $errorLogger;

    public function __construct(ContentPartner $contentPartner, LoggerInterface $logger)
    {
        $this->contentPartner = $contentPartner;
        $this->errorLogger = $logger;
    }

    public function getWeatherForCity($city){
        if(strlen(trim($city)) > 0) {
            $result = $this->contentPartner->getWeatherForCity($city);
            if($result != false){
                $result['dt'] = WeatherHelper::formatUnixTimeToDateTimeString($result['dt']);
                $result['sys']['sunrise'] = WeatherHelper::formatUnixTimeToTimeString($result['sys']['sunrise']);
                $result['sys']['sunset'] = WeatherHelper::formatUnixTimeToTimeString($result['sys']['sunset']);
            }

            return $result;
        }

        return false;
    }
}